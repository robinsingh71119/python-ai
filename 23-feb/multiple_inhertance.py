class Base1:
    a = 10
    def first(self):
        print('Fn in first Class')

class Base2:
    b = 20
    def second(self):
        print('Fn in Second Class')

class child(Base1,Base2):
    def sayHello(self):
        print('Fn in Third class')

obj1 = child()

obj1.first()
obj1.second()
obj1.sayHello()
print(obj1.a)
obj1.a = 20
print(obj1.a)

