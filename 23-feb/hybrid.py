class GrandParent:
    def __init__(self,name):
        self.n = name
        print(name)
    def gp_(self):
        print('GrandParent')

class Father(GrandParent):
    def father_(self):
        print('Father')
    
class Mother(GrandParent):
    def mother_(self):
        print('Mother')

class child(Father,Mother):
    def child_(self):
        print('Child')

ob = child('jarvis')
