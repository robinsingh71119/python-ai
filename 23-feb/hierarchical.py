class Student:
    clz = "ABC"
    def intro(self,name='',roll_no=''):
        self.n = name
        self.r = roll_no
        print(name,'Registration done.')
    def getInfo(self):
        print('Name : {} . Roll NO : {}'.format(self.n,self.r))

class Account(Student):
    def feeDetails(self,total,pending):
        # print('{}, has Paid Rs {}'.format(self.n,total))
        print('{} ,has  Total Fee Rs {}'.format(self.n,total))
        print('Pending Fee {} of student name {}'.format(pending,self.n))

class Library(Student):
    def issueBook(self,bookName,issuedOn,returnOn):
        print('{} Book details'.format(self.n))
        print('Book Name {} , Issued On {}  Return On {}'.format(bookName,issuedOn,returnOn))

# s2 = Library()
# s2.intro('james',102)
# s2.getInfo()
# s2.issueBook('SST','15-2-21','20-2-21')

d = Account()
d.intro('james',101)
d.feeDetails(1200,100)
