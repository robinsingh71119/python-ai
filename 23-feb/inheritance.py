class Student:
    clz = "ABC"
    def intro(self,name,roll_no):
        self.n = name
        self.r = roll_no
        print(name,'Registration done.')
    def getInfo(self):
        print('Name : {} . Roll NO : {}'.format(self.n,self.r))

# single inheritance
class Library(Student):
    def issueBook(self,bookName,issuedOn,returnOn):
        print('{} Book details'.format(self.n))
        print('Book Name {} , Issued On {}  Return On {}'.format(bookName,issuedOn,returnOn))

# s1 = Student()
# s1.intro('jarvis',101)
# s1.getInfo()

s2 = Library()
s2.intro('james',102)
s2.getInfo()
s2.issueBook('SST','15-2-21','20-2-21')