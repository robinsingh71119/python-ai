class Student:
    # variables  = Data members
    # function = Member funtions
    # Data members
    # Public member
    clz = 'ABC'
    # Private member
    __pincode = '121212'
    # Member funtions
    def __intro(self):
        print('Main function of class')
    def __init__(self):
        print('Initilization')
        print(self.__pincode)
class Account(Student):
    def __init__(self):
        super().intro()
    def details(self):
        print('Welcome to {}. Pincode is {}'.format(self.clz,self.__pincode))
obj = Student()
obj.intro()
# print(obj.clz)

obj1 = Account()
obj1.details()
