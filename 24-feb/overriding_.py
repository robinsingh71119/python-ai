class Student:
    clz = 'ABC'
    def info(self):
        print('Welcome from Clg.')

class Account(Student):
    clz="XYZ"
    def __init__(self):
        super().info()
    def info(self):
        print(super().clz)
        # print('Welcome frmo Account Dept.')

    def accDet(self):
        print('Account Details')

obj = Account()
# print(obj.clz)
# obj.info()