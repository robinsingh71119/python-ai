import re
str = """
    Name is Peter , Roll Number: 9809 , Contact Number: 9898989898
    Name is Aman , Roll Number: 678 , Contact Number: 7878789788
    Name is Jack , Roll Number: 9808 , Contact Number: 7673555555
    Name is Jack , Roll Number: 980 , Contact Number: 7673555555
    Name is Jack , Roll Number: 9808 , Contact Number: 7673555555
"""

name        = re.findall('[a-zA-Z]+ ,',str)
roll_no     = re.findall('\d{4} , |\d{3} ,',str)
contact_no  = re.findall('\d{10} \n',str)

print(name)
print(roll_no)
print(contact_no)