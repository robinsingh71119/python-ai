# Paramete arguments
# Positional
# keyword
# Default
# Arbitrary 
    # args
    # kwargs

# Positional
# def addRecord(name,email):
#     print('Name : {} , Email : {}'.format(name,email))

# addRecord('jarvis','jarvis@gmail.com')
# addRecord('jarvis@gmail.com','jarvis')

# keyword
def addRecord(name,email,roll_no):
    print('Name : {} , Email : {} , Roll no : {}'.format(name,email,roll_no))

addRecord(email='jarvis@gmail.com',name='jarvis',roll_no=101)
# addRecord(roll_no=101,name='jarvis',email='jarvis@gmail.com')
# addRecord(roll_no=101,name='jarvis',email='jarvis@gmail.com')

addRecord('jarvis',email='jarvis@gmail.com',roll_no=101) # combination of positional and keyword



