option = '''
    Press 1: To add User
    Press 2: To view Users
    Press 3: To exit
'''
print(option)
users = []
while True:
    choice = input('Enter option. ')
    if choice == '1':
        addUser = input('Enter New User name. ')
        if addUser in users:
            print('{},name is already in the list'.format(addUser))    
        else:
            users.append(addUser)
            print('{},new name added successfully'.format(addUser))
    elif choice == '2':
        print(users)
    elif choice == '3':
        break
    else:
        print('Invalid option')
    print(option)