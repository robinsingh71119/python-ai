# for i in 'python':
#     print(i)

# ab = [i for i in 'python']
# for i in range(len(ab)):
#     print(ab[i])

# list with number from 1 to 100
# ab = [i for i in range(101)]
# list with number from 1 to 100 divisible by 8
# ab = [i for i in range(101) if i%8==0]
# list with number from 1 to 100 divisible by 8 and 4
# ab = [i for i in range(101) if i%3==0 | i%8==0]
# show even odd from 1 to 100
# ab = [['Even =',i] if i%2==0 else ['Odd =',i] for i in range(101)]

# show square of number from  1 to 10
# ab = [i * i for i in range(10)]

str_ = 'sdfhs213!@#!@sdfsdf 12313FSDFSDF324234@#$@#$@sfsdfds'
ab = [i for i in str_ if i.isdigit()]

print(ab)