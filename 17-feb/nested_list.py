data = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
]
# Transpose of matrix
newData = [[0,0,0],[0,0,0],[0,0,0]]
for i in range(len(data)):
    for j in range(len(data[i])):
        newData[i][j] = data[j][i]
        # print('data[{}][{}] => {}'.format(i,j,data[i][j]))

print(newData)