set1 = {23,4,54,54,46,7,12,32,4,756}
set2 = {23,123,2,3,23,7,54,46,2,3,4}

# Union
# unique = set1.union(set2)
unique = set1|set2
# print(unique)

# Intersection
# inset_ = set1.intersection(set2)
inset_ = set1&set2
# print(inset_)


# Difference
# diff = set1.difference(set2)
diff = set1-set2
print(diff)

# Symmetric Difference  
symm_diff = set1.symmetric_difference(set2)
print(symm_diff)