txt = 'HellO aa everyone'
# print(txt)
# print(len(txt))

# print(txt.upper())
# print(txt.lower())
# print(txt.capitalize())
# print(txt.title())
# print(txt.swapcase())

# string type check
# print(txt.isupper())
# print(txt.islower())
# print('All num = ',txt.isalnum())
# print(txt.isalpha())
# print('aa' not in txt)

# indexing
# print(txt[0])
# print(txt[-1])

# slicing
# print('slicing')
# print(txt[3:])
# print(txt[:5])
# print(txt[2:5])
# print(txt[:-5])

#step size
# print('step size')
# print(txt[::2]) # step size [initial:final:steps]


# if 'aa' in txt:
#     print('Success')
# else:    
#     print('asd')

# concatenation
txt1 = ' New string'
print(txt + txt1)

# split
n = txt.split('a')
print(type(n))


