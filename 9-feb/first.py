# Data types
a = 10
b ='I am a code'
c = 10.33
d = [0,1,2,3,4,5,6,6] #array . List mutable=change 
e = ('12','12','dsd') # immutable=not change
f = {'key':'value','key':[],'key':()} # object . dictionary 
# g = {}
gg = set()
cc = False # boolean

# {1231,13248,1,87}
# display
print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(g))
# print(Hello)
