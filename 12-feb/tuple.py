dd = 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
t = (10,2)
# print(type(dd))

# # Indexing
# print(dd[1])
# print(dd[-1])
# print(len(dd))

# Is tuple Immutable ? yes
# dd[1] = '12'
# del dd[2]


# slicing
print(dd) 
# print(dd[2:])
# print(dd[:4])
# print(dd[:-1])
# print(dd[2:-1])

# step size
print(dd[::2])
print(dd[1::2])




