options = '''
    Press 1: To add User
    Press 2: To view Users
    Press 3: To delete User
    Press 4: To Update User
    Press 5: To exit
'''
print(options)
users = []
while True:
    choice = input("Enter Choice: ")
    if choice == "1":
        name = input("Enter Your Name: ")
        if name in users:
            print('{},name is already in the list'.format(name))    
        else:
            users.append(name)
            print('{},added successfully'.format(name))
    elif choice == "2":
        print("Total: ",len(users)) 
        for i in range(len(users)):
            print("index: {} , Name: {}".format(i,users[i]))
    elif choice == "3":
        delName = input("Enter Name to delete: ")
        if delName in users:
            del users[users.index(delName)]
            print('Name deleted successfully')
        else:
            print('{},name is not in the list'.format(delName))
    elif choice == '4':
        oldName = input('Enter Old name: ')
        if oldName in users:
            newName = input('Enter Updated Name')
            users[users.index(oldName)] = newName
            print('{}, name Updated successfully: '.format(newName))
        else:
            print('{}, name not in list. '.format(oldName))
    elif choice == "5":
        break 
    else:
        print("Invalid Choice")
    print(options)
        