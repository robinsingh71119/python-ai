dic_ = {
        'name'  :'Jarvis',
        'sname' :'Singh',
}
student = {
    'name':'Jarvis',
    'roll_no':1,
    'subject':['hindi','maths','gk'],
    'is_present':True,
    'is_failed':None,
    'hobbies':('reading','playing'),
    'technology':{
        'Front_end':['hmtl','css','js','bs'],
        'Back_end':['python','sqlite','tikenter'],
    }
}
# print(type(dic_)) 
# print(dic_) 

# # show data

# print(dic_['name'])

# print('Hobbies => ' ,student['hobbies'][1])
# print(student['technology']['Back_end'][1])

# loop
for i in student:
    print(i,':',student[i])