student = {
    'name':'Jarvis',
    'roll_no':1,
    'subject':['hindi','maths','gk'],
    'is_present':True,
    'is_failed':None,
    'hobbies':('reading','playing'),
    'technology':{
        'Front_end':['hmtl','css','js','bs'],
        'Back_end':['python','sqlite','tikenter'],
    }
}

# newRec = student.copy()

# print(student.keys())
# print(student.values())
# print(student.items())

# insert element
student['leaves'] = 12
# update
# student['is_present'] = False

student.update({'is_present':False})


# Delete
student.pop('name')
del student['hobbies']

for k,v in student.items():
    print('{} : {}'.format(k,v))