class Area:
    pi = 3.14

    def radiusSet(s,r):
        s.r = r
        print('Radius set')
        
    def circleArea(s):
        print('Area Of circle with radius is {}'.format(s.pi*s.r**2))

cc = Area()
cc.radiusSet(12)
cc.circleArea()