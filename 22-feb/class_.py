class Abc:
    # data member
    name = ''

    # member function
    def setName(self,name):
        self.name = name

    def show(self):
        # print(self.name)
        print('{}, Hi Welcome to our code'.format(self.name))
        

h = Abc()
h.setName('jarvis')
h.show()
h.setName('james')
h.show()
