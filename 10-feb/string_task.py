num = input('Enter Your number')
if num.isdigit():
    if len(num) >= 10:
        print(len(num[:-3])*"*"+num[-3:])
    else:
        print('10 digits required')
else:
    print('Invalid number')

