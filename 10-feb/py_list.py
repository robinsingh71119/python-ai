# Array []
l = [1,2,3,4,5]
# print(type(l))
# print(l)

# Indexing
# print(l[2])
# print(l[len(l)-2])
# print(l[-2]) # reverse indexing

color = ['red','green','blue','orange','pink','gray']

# slicing
# print(color[2:]) #accept the index value
# print(color[:3]) #not accept the index value
# print(color[5:10])

# step size
# print(color[1:10:2])

# Reverse a list using step size
# print(color[::-1])

# Concatenate
# new_s = l+color
# print(len(new_s))

# Membership
print('Gray' in color)
print('Gray' not in color)
 